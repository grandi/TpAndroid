package com.example.valeria.tpandroid;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.valeria.tpandroid.modelo.Pais;
import com.example.valeria.tpandroid.tools.ApplicationToolset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListaDePaisesController extends BaseObservable {
    private AppCompatActivity activity;

    public AppCompatActivity getActivity() {
        return this.activity;
    }

    private List<Pais> paises = new ArrayList<>();
    private ArrayAdapter<Object> paisesAdapter;
    private SimpleAdapter segundoAdapter;

    public ListaDePaisesController(AppCompatActivity _activity) {
        super();
        this.activity = _activity;
        this.createPaisesAdapter();
        this.segundoAdapter=null;
        this.paisesAdapter.add("... obteniendo información ...");
        this.fetchPaises();
    }

    @Bindable
    public BaseAdapter getPaisesAdapter(){
        if(this.segundoAdapter == null){
            return this.paisesAdapter;

        }else {
            return this.segundoAdapter;
        }
    }

    public void createPaisesAdapter() {
        this.paisesAdapter = new ArrayAdapter<>(
                this.activity, android.R.layout.simple_list_item_1
        );
    }

    public void createSegundoAdapter(){
        this.segundoAdapter = new SimpleAdapter(
                this.activity,
                this.crearMapasAMostrar(),
                android.R.layout.simple_list_item_2,
                new String[]{"name","region"},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        this.notifyPropertyChanged(BR.paisesAdapter);
    }
    public List<Map<String, String>> crearMapasAMostrar() {
        ArrayList<Map<String, String>> losMapas = new ArrayList<>();

        for (Pais pais: paises) {
            Map<String, String> infoPais = new HashMap<>();
            infoPais.put("name", pais.getNombre());
            infoPais.put("region", pais.getContinente());
            losMapas.add(infoPais);
        }
        return losMapas;
    }
   public void llenarPaisesAdapter() {
        this.paisesAdapter.clear();
        this.createSegundoAdapter();
    }

    public void fetchPaises() {
        String url = "https://restcountries.eu/rest/v2/region/americas";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (
                        Request.Method.GET,
                        url,
                        null,
                        (JSONArray response) -> {
                            this.procesarRespuestaDelServer(response);
                            this.llenarPaisesAdapter();
                        },
                        (VolleyError error) -> {
                            error.printStackTrace();
                            throw new RuntimeException("Error en el request REST", error);
                        }
                );

        ApplicationToolset.toolset().addToRequestQueue(jsonArrayRequest);
    }

    public void procesarRespuestaDelServer(JSONArray respuesta) {
        try {

            for (int indice = 0; indice < respuesta.length(); indice++) {

                JSONObject pais = respuesta.getJSONObject(indice);

                String name = pais.getString("name");

                String capital = pais.getString("capital");

                int poblacion= pais.getInt("population");

                String continente= pais.getString("region");

                JSONArray idiomas= pais.getJSONArray("languages");
                JSONObject lenguaje = idiomas.getJSONObject(0);
                String elIdioma= lenguaje.getString("nativeName");


                List<String> limites = this.parsearLimites(pais);

                List<String>dominios= this.parsearDominios(pais);

                this.paises.add(new Pais(name, capital,poblacion,continente,dominios,
                        elIdioma,limites));


            }
        } catch (JSONException error) {
            error.printStackTrace();
            throw new RuntimeException("Error en el procesamiento del JSON", error);
        }
    }

    private List<String> parsearLimites(JSONObject pais) {
        try {
            List<String> limites = new ArrayList<>();
            JSONArray limitesJson = pais.getJSONArray("borders");

            for (int indice = 0; indice < limitesJson.length(); indice++) {
                String limite = limitesJson.getString(indice);
                limites.add(limite);

            }

            return limites;

        } catch (JSONException error) {
            error.printStackTrace();
            throw new RuntimeException("Error en el procesamiento del JSON", error);
        }
    }
    private List<String> parsearDominios(JSONObject pais) {
        try {
            List<String> dominios = new ArrayList<>();
            JSONArray dominiosJson = pais.getJSONArray("topLevelDomain");

            for (int indice = 0; indice < dominiosJson.length(); indice++) {
                String dominio = dominiosJson.getString(indice);
                dominios.add(dominio);

            }

            return dominios;

        } catch (JSONException error) {
            error.printStackTrace();
            throw new RuntimeException("Error en el procesamiento del JSON", error);
        }
    }

    public Pais getPais(int position) {
        Pais laPosicion = this.paises.get(position);
        return laPosicion;
    }


    public void mostrarPais(int position) {
        Intent salto = new Intent(this.activity, PaisActivity.class);
        salto.putExtra("name",getPais(position));
        this.activity.startActivity(salto);
    }

}




