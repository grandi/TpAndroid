package com.example.valeria.tpandroid;

import android.databinding.BaseObservable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.example.valeria.tpandroid.modelo.Pais;

import java.util.List;

public class PaisController extends BaseObservable {
    private AppCompatActivity activity;
    private Pais paisAMostrar;
    private String titulo = "Mas datos de: ";
    public String limite;

    public PaisController(AppCompatActivity _activity) {
        this.activity = _activity;
    }

    public String getPaisAMostrar() {
        return this.paisAMostrar.getPais();
    }

    public void setPaisAMostrar(Pais _pais) {
        this.paisAMostrar = _pais;
    }

    public String getCapital(){
        return this.paisAMostrar.getCapital();
    }

    public String getPoblacion() {
        return this.paisAMostrar.getPoblacion();
    }

    public String getTitulo() {
        return this.titulo;
    }

    public String getDominioInternet() {
        return TextUtils.join("; ",this.paisAMostrar.getDominio());
    }

    public String getIdiomas() {
        return this.paisAMostrar.getIdioma();
    }


    public String getLosLimites() {

        if(this.paisAMostrar.getLimites().isEmpty()){
            return "no hay límites a mostrar";
        }
        else {
            return TextUtils.join(", ", this.paisAMostrar.getLimites());
        }

    }
}

