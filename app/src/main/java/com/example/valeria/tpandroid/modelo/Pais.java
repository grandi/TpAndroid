package com.example.valeria.tpandroid.modelo;

import java.io.Serializable;
import java.util.List;

public class Pais implements Serializable {
    private String nombre;
    private String capital;
    private int poblacion;
    private String continente;
    private List<String> dominio;
    private String idioma;
    private List<String> limites;


    public Pais(String _nombre, String _capital, int _poblacion, String _continente,
                List<String> _dominio, String _idioma, List<String> _limites) {
        this.nombre = _nombre;
        this.capital = _capital;
        this.poblacion = _poblacion;
        this.continente = _continente;
        this.dominio = _dominio;
        this.idioma = _idioma;
        this.limites=_limites;

    }


    public String getPais() {
        return String.valueOf(this.nombre);
    }

 public String getNombre() {
        return nombre;
    }

    public String getCapital() {
        return this.capital;
    }

    public String getPoblacion() {
        return String.valueOf(this.poblacion);
    }

    public String getContinente() {
        return this.continente;
    }

    public List<String> getDominio() {
        return this.dominio;
    }

    public String getIdioma() {
        return this.idioma;
    }

    public List<String> getLimites() {
        return this.limites;
    }
}