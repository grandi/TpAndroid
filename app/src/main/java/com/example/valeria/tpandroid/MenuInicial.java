package com.example.valeria.tpandroid;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.valeria.tpandroid.databinding.ActivityMenuInicialBinding;

import com.example.valeria.tpandroid.tools.ApplicationToolset;


public class MenuInicial extends AppCompatActivity {

    MenuInicialController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ApplicationToolset.setContext(this.getApplicationContext());

        super.onCreate(savedInstanceState);
        controller = new MenuInicialController(this);
        ActivityMenuInicialBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_menu_inicial);
        binding.setController(this.controller);
    }
}
