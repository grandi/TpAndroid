package com.example.valeria.tpandroid;

import android.app.Application;

import com.jakewharton.threetenabp.AndroidThreeTen;

public class PruebasApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
    }
}
