package com.example.valeria.tpandroid;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;

public class MenuInicialController extends BaseObservable {

    private AppCompatActivity activity;
    private String titulo=" Los países del continente americano";

    public AppCompatActivity getActivity() {
        return this.activity;
    }

    public MenuInicialController(AppCompatActivity _activity){
        super();
        this.activity=_activity;
    }
   public void mostrarListaDePaises() {
        Intent salto = new Intent(this.activity, ListaDePaisesActivity.class);
        this.activity.startActivity(salto);
    }

 @Bindable
    public String getTitulo() { return this.titulo; }



}