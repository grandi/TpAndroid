
package com.example.valeria.tpandroid;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.valeria.tpandroid.databinding.ActivityListaDePaisesBinding;
import com.example.valeria.tpandroid.modelo.Pais;

public class ListaDePaisesActivity extends AppCompatActivity {

    private ListaDePaisesController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Object paisInicial = this.getIntent().getSerializableExtra("name");

        controller = new ListaDePaisesController(this);
        ActivityListaDePaisesBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_lista_de_paises);
        binding.setController(controller);

        this.agregarOnClick();
        this.volverAtras();
    }

    private void agregarOnClick() {
        ListView datosPais = this.findViewById(R.id.listaDePaises);
        datosPais.setOnItemClickListener((listView, viewFila, position, elIdDeNoSeQue) -> {
            this.controller.mostrarPais(position);
        });
    }

    private void volverAtras() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("volver al inicio");

        }
    }
}



