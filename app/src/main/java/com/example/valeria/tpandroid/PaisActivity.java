
package com.example.valeria.tpandroid;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.example.valeria.tpandroid.databinding.ActivityListaDeDatosPaisBinding;
import com.example.valeria.tpandroid.modelo.Pais;

public class PaisActivity extends AppCompatActivity {
    private PaisController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new PaisController(this);
        ActivityListaDeDatosPaisBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_lista_de_datos_pais);
        binding.setController(this.controller);

        Intent paseRecibido = this.getIntent();
        Pais paisAMostrar = (Pais) paseRecibido.getSerializableExtra("name");
        controller.setPaisAMostrar(paisAMostrar);

        volverAtras();


    }

    private void volverAtras() {
        ActionBar actionBar=  getSupportActionBar();
            if(actionBar !=null){
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle("volver al listado");
                
        }
    }

}






